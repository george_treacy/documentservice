package co.redeye.core.documents;

/**
 * Created on 2/09/2015.
 */
public class Bob {

    public Bob() {}

    private String fname;
    private String lname;

    public String getFname() {
        return fname;
    }

    public void setFname(String fname) {
        this.fname = fname;
    }

    public String getLname() {
        return lname;
    }

    public void setLname(String lname) {
        this.lname = lname;
    }
}
