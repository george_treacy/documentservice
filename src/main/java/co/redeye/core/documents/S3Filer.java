package co.redeye.core.documents;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

@Path("metadata")
public class S3Filer {

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Bob getMetadata(DocumentRequest documentRequest) {

        System.out.println(documentRequest.getBucket());

        Bob bob = new Bob();
        bob.setFname("Bob");
        bob.setLname("Bitching");
        return bob;
    }

}
